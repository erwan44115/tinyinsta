﻿# Projet Web & Cloud <br/> Groupe : DURAND Erwan, GIRARD Lucie, ANTHEAUME Clément
Réalisation d'un Tiny Instagram.
Nous avons réussi à implémenter toutes les fonctions demandées.
Pour tester l'application, un compte qui follow quelqu'un ayant déjà fait un poste vous a été attribué.

    Login : pascalmolli
    Mdp : supermdp

![alt text](https://cdn.discordapp.com/attachments/576422257606524939/655830426484604948/unknown.png)

Mais vous pouvez aussi créer un compte à votre guise.

URL Google App Engine de l'application déployée : https://scenic-patrol-257112.appspot.com <br/>
URL permettant d'explorer l'interface REST :  https://apis-explorer.appspot.com/apis-explorer/?base=https%3A%2F%2Fscenic-patrol-257112.appspot.com%2F_ah%2Fapi&root=https%3A%2F%2Fscenic-patrol-257112.appspot.com%2F_ah%2Fapi#p/tinyAPI/v1/

Commandes pour utiliser l'application en local : 

    - Dans le dossier \src\main\ngapp :  npm install
    - Dans le dossier  \src\main\ngapp\tiny : ng build
    - A la racine du projet : mvn appengine:run

-------------------------------------------------------------------------------------------------

# Scaling du compteur de like

Pour scale le compteur de like, nous avons décidé de créer une entité spécialement pour le like.
Nous avons donc une entité Poste avec les attributs suivants : 
 
#### Kind Poste
    - idAuteur
    - message
    - url

Et nous avons ensuite comme entité enfant de ce poste une entité ShardedLike (Poste est donc ancestre de ShardedLike)
qui lui possède un attribut NumberOfCpt pour savoir le nombre de compteurs, et n compteurs selon les besoins de scale ledit poste. 
Par exemple pour un NumberOfCpt de 10 on aura : 
     
     
#### Kind ShardedLike
     - NumberOfcpt 
     - cpt0
     - cpt1
     - cpt2
     - cpt3
     - cpt4
     - cpt5
     - cpt6
     - cpt7
     - cpt8
     - cpt9
     
Ainsi lorsque l'on incrémente le compteur de like, on choisira un cpt au hasard à incrémenter, ce qui réduit énormément la contention. 
Pour retrouver le nombre total de like pour un poste, il suffit alors de faire la somme de tous les cpt. 

# Scaling des postes

Pour le scalling des postes, nous avons fait en sorte que, lorsque quelqu'un fait un nouveau poste, cette personne n'ait pas besoin d'envoyer les postes à tous ses followers. 
En effet, ce sont les followers eux-même qui, quand ils chargent leur timeline, sont chargés d'aller chercher les derniers posts, car ce sont eux qui possèdent une liste de personnes qu'ils suivent.

#### Kind User
    - id
    - listFollow
    - login
    - mdp

-------------------------------------------------------------------------------------------------    

# Screenshots des "Kinds" Google Datastore utilisés dans notre application

![alt text](https://cdn.discordapp.com/attachments/540888354968043520/655729240733515777/unknown.png)

![alt text](https://cdn.discordapp.com/attachments/540888354968043520/655729396350582784/unknown.png)

![alt text](https://cdn.discordapp.com/attachments/540888354968043520/655729555096731669/unknown.png)

-------------------------------------------------------------------------------------------------

# Valeurs de test de scaling

Nous avons implémentés des fonctions de test pour évaluer le niveau de scaling en mesurant la durée pour : 
- poster à un nombre variable de followers
- récupérer un nombre variable de derniers posts
- évaluer le nombre de likes possibles par seconde

Les deux premiers points ont été abordés sans thread, donc sans parallélisme car nous avons rencontré des problèmes pour le troisième et dernier point. <br/>
En effet, nos threads "crashaient" à partir d'un certain point lorsque l'on testait les likes d'un post, sans que l'on y trouve de solution malgré le fait que nous pensons que notre solution est bonne pour le scaling, de même qu'elle permet une incrémentation safe pour notre compteur. <br/>
Nous n'avons donc pas trouvé utile de répondre au troisième point sans utiliser de thread. <br/> 
Nous détaillons donc ci-dessous seulement nos résultats concernant les deux premiers points.

### Durées pour poster

Notre implémentation permet de poster à : 
1. 10 followers en un temps de 0,200s.
2. 200 followers en un temps de 0,200s.
3. 500 followers en un temps de 0,200s.

En effet, le temps est le même quelque soit le nombre de followers puisque notre méthode consiste à ce que ce soit les followers qui récupèrent les posts.

### Durées pour récupérer les derniers posts

Notre implémentation permet de récupérer :
1. 10 posts en un temps de 0,200s.
2. 200 posts en un temps de 2,769s.
3. 500 posts en un temps de 2,920s.

On remarque ici que le temps augmente considérablement lorsque l'on doit récupérer un nombre plus important de posts. <br/>
Nous ne pensons pas que ce soit critique car, en réalité, sur Facebook ou Twitter par exemple, ces applications n'affichent pas plus de quelques dizaines de posts à la fois, ce n'est qu'à la fin du scolling que l'application charge les prochains.

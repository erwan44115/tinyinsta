var app = angular.module('TinyInsta', []);
app.controller('Controller', ['$scope', '$window', function ($scope, $window) {

// VARIABLE INITIALIZATION

    $scope.name = "World"
    $scope.showlogin = true;
    $scope.connected = false;
    $scope.errorLogin = false;
    $scope.errorRegister = false;
    $scope.affichageLogin = [];
    $scope.postList = [];
    $scope.askedTL = false;

// BEGIN FUNCTION

    $scope.newPost = function (url, newDesc) {
        gapi.client.tinyAPI.newPost({
            url: url,
            newDesc: newDesc,
            author: $scope.pseudo
        }).execute(function (resp) {
            console.log("test");
            console.log(resp);

        });
    }

    $scope.getTimeline = function (pseudo) {
        gapi.client.tinyAPI.getTimeline(
            {login: pseudo}
        ).execute(function (resp) {
            $scope.postList = [];
            $scope.askedTL = true;
            console.log("Test Timeline");
            console.log(pseudo);
            console.log(resp.result);
            if (resp.items.length > 0) {
                for (var i = 0; i < resp.items.length; i++) {
                    $scope.postList.push({
                        date: resp.items[i].properties.date,
                        idAuteur: resp.items[i].properties.idAuteur,
                        message: resp.items[i].properties.message,
                        url: resp.items[i].properties.url
                    });
                }
            }
        });
    }
}])
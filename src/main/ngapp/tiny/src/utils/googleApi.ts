import {Error} from "../app/error";

declare var gapi: any;

export class GoogleApi {
  static sendRequest(routeName, params, callback) {
    if (gapi.client == undefined || gapi.client.tinyAPI == undefined) {
      console.error("[GOOGLE] - Client non initialisé");
      throw "Imposible de joindre l'API Google";
    }

    if (gapi.client.tinyAPI[routeName] == undefined) {
      console.error("[GOOGLE] - Route " + routeName + " non trouvée");
      throw "Imposible de joindre l'API Google";
    }

    gapi.client.tinyAPI[routeName](params).execute((resp) => {
      console.log(resp)
      callback(resp)
    });
  }
}

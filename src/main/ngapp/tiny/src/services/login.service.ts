import { Injectable } from '@angular/core';
import {Login} from "../app/login";
import {User} from "../app/user";

@Injectable()
export class LoginService {

  private login :Login|null;

  // Service message commands
  setLogin(login: Login|null) {
    this.login = login;
    return this;
  }

  getUsername(): string|null {
    return (this.login == null) ? null : this.login.username;
  }

  isLogged(): boolean {
    return this.login != null;
  }

  getUser(): User {
    return this.login.getUser();
  }

  reset() {
    this.login.setUser(null);
  }
}

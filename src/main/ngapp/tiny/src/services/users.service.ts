import {Injectable} from '@angular/core';
import {User} from "../app/user";
import {variable} from "@angular/compiler/src/output/output_ast";

@Injectable()
export class UsersService {

  private users: User[] = [];

  getUsers(): User[] {
    return this.users;
  }

  addUser(user: User) {
    this.users.push(user);
  }

  setUsers(users: User[]) {
    this.users = users;
  }

  reset() {
    this.users = []
  }

}

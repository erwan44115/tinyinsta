import {Injectable} from '@angular/core';
import {Error} from "../app/error";

@Injectable()
export class ErrorsService {

  private errors: Error[] = [];

  getErrors(): Error[] {
    return this.errors;
  }

  addError(error: Error) {
    this.errors.push(error);
  }

  removeError(error: Error) {
    let index = this.errors.indexOf(error)
    if (index != -1) {
      this.errors.splice(index, 1);
    }
  }
}

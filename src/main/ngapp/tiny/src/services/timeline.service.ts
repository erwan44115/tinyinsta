import {Injectable} from '@angular/core';
import {Post} from "../app/post";

@Injectable()
export class TimelineService {

  private posts: Post[] = [];

  getPosts(): Post[] {
    return this.posts;
  }

  addPost(post: Post) {
    this.posts.push(post);
  }

  reset() {
    this.posts = []
  }
}

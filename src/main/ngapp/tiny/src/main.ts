import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import $ from 'jquery';

declare var gapi :any;

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));


// TODO try to fix this
setTimeout(() => {
  gapi.client.load('tinyAPI', 'v1', function () {
    console.log("[APP] - Api tinyAPI loaded");
  }, 'https://scenic-patrol-257112.appspot.com/_ah/api/');
}, 1000)

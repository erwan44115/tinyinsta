import {Component, NgZone, OnInit} from '@angular/core';

import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Post} from "../post";
import {Error} from "../error";
import {GoogleApi} from "../../utils/googleApi";
import {LoginService} from "../../services/login.service";
import {ErrorsService} from "../../services/errors.service";

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  post :Post = new Post();

  constructor(
    private loginService: LoginService,
    private errorsService: ErrorsService,
    private ngZone: NgZone,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then(() => {
      if(this.post.url == undefined || this.post.description == undefined) {
        return;
      }

      GoogleApi.sendRequest("newPost", {
        url: this.post.url,
        newDesc: this.post.description,
        author: this.loginService.getUsername()
      }, (resp) => {
        this.ngZone.run(() => {
          this.post = new Post();
          this.errorsService.addError(new Error("Image publiée", "success"))
        })
      })
    });
  }
}

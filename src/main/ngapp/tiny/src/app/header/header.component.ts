import {Component, EventEmitter, NgZone, OnInit, Output} from '@angular/core';
import {GoogleApi} from "../../utils/googleApi";
import {Login} from "../login";
import {Error} from "../error";
import {LoginService} from "../../services/login.service";
import {ErrorsService} from "../../services/errors.service";
import {User} from "../user";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() loggingIn = new EventEmitter<void>();
  @Output() loggingOut = new EventEmitter<void>();

  model = new Login()

  constructor(
    private loginService: LoginService,
    private ngZone: NgZone,
    private errorsService: ErrorsService) {
  }

  ngOnInit() {

  }

  login(event: MouseEvent) {
    // Prevent default to cancel reload after form submit
    event.preventDefault()
    event.stopPropagation()

    if(!this.formValid()) {
      return
    }

    try {
      GoogleApi.sendRequest('connection', {
        login: this.model.username,
        mdp: this.model.password,
      }, (resp) => {
        this.ngZone.run(() => {
          if (!resp.hasOwnProperty('properties')) {
            this.model.password = ''
            this.errorsService.addError(new Error("Identifiants incorrects"));
          } else {
            this.model.setUser(new User(resp.properties))
            this.loginService.setLogin(this.model)
            this.loggingIn.emit();
          }
        })
      })
    } catch (e) {
      this.errorsService.addError(new Error(e));
    }
  }

  logout(event: MouseEvent) {
    // Prevent default to cancel reload after form submit
    event.preventDefault()
    event.stopPropagation()

    this.loginService.setLogin(null);
    this.model.username = ""
    this.model.password = ""
    this.errorsService.addError(new Error("Vous êtes maintenant déconnecté", "success"));
    this.loggingOut.emit();
  }

  register(event: MouseEvent) {
    event.preventDefault()
    event.stopPropagation()

    if(!this.formValid()) {
      return
    }

    try {
      GoogleApi.sendRequest('createUser', {
        login: this.model.username,
        mdp: this.model.password,
      }, (resp) => {
        this.ngZone.run(() => {
          if (!resp.hasOwnProperty('properties')) {
            this.errorsService.addError(new Error("Imposible de vous inscrire"));
          } else {
            this.model.setUser(new User(resp.properties))
            this.loginService.setLogin(this.model)
            this.errorsService.addError(new Error("Vous êtes maintenant inscrit", "success"));
            this.loggingIn.emit();
          }
        })
      })
    } catch (e) {
      this.errorsService.addError(new Error(e));
    }
  }

  private formValid() {
    return this.model.username != undefined && this.model.username.length && this.model.password != undefined && this.model.password.length;
  }
}

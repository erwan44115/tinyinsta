import {Component, NgZone, OnInit} from '@angular/core';
import {GoogleApi} from "../../utils/googleApi";
import {ErrorsService} from "../../services/errors.service";
import {UsersService} from "../../services/users.service";
import {LoginService} from "../../services/login.service";
import {Error} from "../error";
import {User} from "../user";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  followUsername: string;

  constructor(
    private errorsService: ErrorsService,
    private ngZone: NgZone,
    private usersService: UsersService,
    private loginService: LoginService,
  ) {
  }

  ngOnInit() {

  }

  follow(username: string) {
    if (!username.length) {
      return
    }

    try {
      GoogleApi.sendRequest('followSomeone', {
        loginFollowed: username,
        loginMyAccount: this.loginService.getUsername(),
      }, (resp) => {
        this.ngZone.run(() => {
          if (resp.code == 404) {
            this.errorsService.addError(new Error(resp.message));
          }
          this.loginService.getUser().listFollow = resp.properties.listFollow
        })
      });

    } catch (e) {
      this.errorsService.addError(new Error("Imposible de joindre l'API Google"));
      console.error("[APP] Dump Google error", e)
    }
  }

  canFollow(user: User) {
    if(user.username == this.loginService.getUsername()) {
      return false;
    }

    if(this.loginService.getUser().listFollow.indexOf(user.id) != -1) {
      return false;
    }

    return true;
  }
}

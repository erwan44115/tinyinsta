export class User {
  private _id: string;
  private _username: string;
  private _listFollow: string[];

  constructor(userData?: {
    id: string,
    login: string,
    mdp: string,
    listFollow: null|string[]
  }) {
    this._id = userData.id;
    this._username = userData.login;
    this._listFollow = (userData.listFollow == undefined) ? [] : userData.listFollow;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  get listFollow(): string[] {
    return this._listFollow;
  }

  set listFollow(value: string[]) {
    this._listFollow = value;
  }
}

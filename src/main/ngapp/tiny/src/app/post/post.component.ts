import {Component, Input, NgZone, OnInit} from '@angular/core';
import {Post} from "../post";
import {GoogleApi} from "../../utils/googleApi";
import {Error} from "../error";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input() post: Post;

  constructor(private ngZone: NgZone) { }

  ngOnInit() {
  }

  like(idPost: String){
    GoogleApi.sendRequest("incrementLike", {
      idPost: idPost
    }, (resp) => {
      this.ngZone.run(() => {
        this.post.like = this.post.like + 1;
      })
    })
  }
}

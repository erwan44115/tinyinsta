import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import {FormsModule} from "@angular/forms";
import {TimeAgoPipe} from 'time-ago-pipe';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ErrorsComponent } from './errors/errors.component';
import { UsersComponent } from './users/users.component';
import { PostComponent } from './post/post.component';
import { NewPostComponent } from './new-post/new-post.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ErrorsComponent,
    UsersComponent,
    PostComponent,
    NewPostComponent,
    TimeAgoPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule
  ],
  providers: [{provide: APP_BASE_HREF, useValue : '/' }],
  bootstrap: [AppComponent]

})
export class AppModule { }

export class Post {
  private _id: string;
  private _url: string;
  private _idAuteur?: string | null;
  private _like: number;
  private _description: string;
  private _date: Date;

  constructor(id?: string|null, url?: string|null, idAuteur?:string|null, date?:Date|null, description?: string|null, like?: number|null) {
    this._id= id;
    this._idAuteur = idAuteur;
    this._url = url;
    this._like = like;
    this._date = date;
    this._description = description;
  }

  get id(): string{
    return this._id;
  }

  set id(value: string){
    this._id = value;
  }

  get url(): string {
    return this._url;
  }

  set url(value: string) {
    this._url = value;
  }

  get like(): number {
    return this._like;
  }

  set like(value: number) {
    this._like = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get date(): Date {
    return this._date;
  }

  set date(value: Date) {
    this._date = value;
  }

  get idAuteur(): string | null {
    return this._idAuteur;
  }

  set idAuteur(value: string | null) {
    this._idAuteur = value;
  }
}

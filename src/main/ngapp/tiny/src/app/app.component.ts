import {Component, NgZone, OnInit} from '@angular/core';
import {GoogleApi} from "../utils/googleApi";
import {LoginService} from "../services/login.service";
import {ErrorsService} from "../services/errors.service";
import {TimelineService} from "../services/timeline.service";
import {UsersService} from "../services/users.service";
import {Post} from "./post";
import {User} from "./user";
import {Error} from "./error";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [LoginService, ErrorsService, TimelineService, UsersService]
})
export class AppComponent implements OnInit {
  cptLike = 0;
  constructor(
    private loginService: LoginService,
    private errorsService: ErrorsService,
    private timelineService: TimelineService,
    private ngZone: NgZone,
    private usersService: UsersService) {
  }

  ngOnInit(): void {
    console.log("[APP] - App load ");
  }

  onLogging() {
    try {
      GoogleApi.sendRequest('get10firstUser', {}, (resp) => {
        this.ngZone.run(() => {
          for (let i = 0; i < resp.items.length; i++) {
            this.usersService.addUser(new User(resp.items[i].properties))
          }
        })
      });
      GoogleApi.sendRequest('getTimeline', {
        login: this.loginService.getUsername()
      }, (resp) => {
        this.ngZone.run(() => {

          for (let i = 0; i < resp.items.length; i++) {
            GoogleApi.sendRequest('getLikeNumber', {idPost: resp.items[i].key.id}, (resp2) => {
              this.ngZone.run(() => {
                this.cptLike = resp2.items[0];
                console.log(this.cptLike);
                this.timelineService.addPost(new Post(resp.items[i].key.id,resp.items[i].properties.url, resp.items[i].properties.idAuteur, new Date(resp.items[i].properties.date), resp.items[i].properties.message, this.cptLike));
              })
              console.log("second log : "+this.cptLike);
            });
          }

          console.log(this.timelineService.getPosts());
        })
      });
    } catch (e) {
      this.errorsService.addError(new Error("Imposible de joindre l'API Google pour recupérer la liste des utilsateurs"));
      console.error("[APP] Dump Google error", e)
    }
  }

  onLoggingOut() {
    this.usersService.reset()
    this.timelineService.reset()
    this.loginService.reset()
  }

  delay(ms: number) {
      return new Promise( resolve => setTimeout(resolve, ms) );
  }
}

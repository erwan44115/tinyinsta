export class Error {
  private readonly _message: string;
  private readonly _type: string;

  constructor(message :string, type :string = "danger") {
    this._message = message;
    this._type = type;
  }

  get message(): string {
    return this._message;
  }

  get type(): string {
    return this._type;
  }

}

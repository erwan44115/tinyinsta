import {User} from "./user";

export class Login {
  public username: string;
  public password: string;
  private _user: User;

  constructor(username?: string, password?: string) {
    this.username = username;
    this.password = password;
  }

  setUser(user: User) {
    this._user = user;
  }

  getUser() {
    return this._user;
  }
}

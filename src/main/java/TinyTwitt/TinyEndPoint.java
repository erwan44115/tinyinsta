package TinyTwitt;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.ThreadManager;
import com.google.appengine.api.datastore.*;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.FetchOptions;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;
import com.sun.webkit.dom.EntityImpl;

import java.util.*;
import java.lang.Long;
import java.lang.reflect.Array;
import java.util.Date;
import java.util.ArrayList;
import static java.lang.Math.toIntExact;
import java.util.List;
import java.util.ArrayList;



@Api(name = "tinyAPI", version = "v1",namespace = @ApiNamespace(ownerDomain = "https://endpointsportal.scenic-patrol-257112.cloud.goog", ownerName = "tiny.com", packagePath = ""))
public class TinyEndPoint {

    @ApiMethod(name = "createUser", httpMethod = ApiMethod.HttpMethod.POST)
    public Entity createUser(@Named("login") String login, @Named("mdp") String mdp) {
        if(isLoginFree(login)) {
            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            Entity user = new Entity("User");
            user.setIndexedProperty("login", login);
            user.setProperty("mdp", mdp);
            user.setProperty("listFollow", new ArrayList<Long>());
            datastore.put(user);
            user.setProperty("id", user.getKey().getId());
            datastore.put(user);
            return user;
        }
        return null;
    }

    private boolean isLoginFree(String login){
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Filter filterlogin = new Query.FilterPredicate("login", Query.FilterOperator.EQUAL, login);
        Query query = new Query("User").setFilter(filterlogin);
        Entity user = datastore.prepare(query).asSingleEntity();
        return user == null;
    }

    @ApiMethod(name = "connection", httpMethod = ApiMethod.HttpMethod.POST)
    public Entity connection(@Named("login") String login, @Named("mdp") String mdp) throws Exception {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Filter filterlogin = new Query.FilterPredicate("login", Query.FilterOperator.EQUAL, login);
        Filter filtermdp = new Query.FilterPredicate("mdp", Query.FilterOperator.EQUAL, mdp);
        Query query = new Query("User").setFilter(filterlogin);
        query.setFilter(filtermdp);
        Entity user = datastore.prepare(query).asSingleEntity();
        System.out.println(user);
        return user;
    }

    @ApiMethod(name = "getTimeline", httpMethod = ApiMethod.HttpMethod.GET)
        public List<Entity> getTimeline(@Named("login") String login) throws Exception{
            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            Filter filterlogin = new Query.FilterPredicate("login", Query.FilterOperator.EQUAL, login);
            Query query = new Query("User").setFilter(filterlogin);
            Entity user = datastore.prepare(query).asSingleEntity();
            Long id = user.getKey().getId();

            List<Entity> postList = new ArrayList<Entity>();
            ArrayList<Long> listeF = (ArrayList<Long>) user.getProperty("listFollow");
            if (listeF == null){
                return postList;
            }
            for (Long idFollower : listeF){
                Filter filterposts = new Query.FilterPredicate("idAuteur", Query.FilterOperator.EQUAL, idFollower);
                Query queryposts = new Query("Post").setFilter(filterposts);
                List<Entity> posts = datastore.prepare(queryposts).asList(FetchOptions.Builder.withDefaults());
                if(posts != null){
                    postList.addAll(posts);
                }
            }
            return postList;
    }

    @ApiMethod(name = "newPost", httpMethod = ApiMethod.HttpMethod.POST)
    public Entity newPost(@Named("url") String url, @Named("newDesc") String newDesc, @Named("author") String pseudo) {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Filter filterlogin = new Query.FilterPredicate("login", Query.FilterOperator.EQUAL, pseudo);
        Query query = new Query("User").setFilter(filterlogin);
        Entity user = datastore.prepare(query).asSingleEntity();
        Entity poste = new Entity("Post");
        Date date = new Date();
        poste.setProperty("date", date);
        poste.setProperty("idAuteur", user.getKey().getId());
        poste.setProperty("url", url);
        poste.setProperty("message", newDesc);;
        datastore.put(poste);
        newShardedCounter(datastore,poste,28); // permet de créer l'entité pour le like associé au poste
        return poste;
    }



    /*
    Fonction pour créer et associer, à un poste, un compteur de like qui va bien scale
    Faire varier la variable "numberOfCpt" va permettre d'augmenter le scaling potentiel
     */
    private void newShardedCounter(DatastoreService datastore, Entity poste, int numberOfCpt){
        Entity shardedCounter = new Entity("ShardedLike",poste.getKey()); // le 2ème argument permet de mettre shardedCounter comme entité enfant du poste (et donc d'associer le compteur au poste)
        String properties;
        for(int i=0; i<numberOfCpt; i++){
            properties="cpt"+i;
            shardedCounter.setProperty(properties,0);
        }
        shardedCounter.setProperty("NumberOfCpt",numberOfCpt);
        System.out.println(shardedCounter);
        datastore.put(shardedCounter);
    }

    /*
     Obtenir le nombre de like d'un poste
     */
    @ApiMethod(name = "getLikeNumber", httpMethod = ApiMethod.HttpMethod.GET)
    public long[] getLikeNumber(@Named("idPost") String idPost) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        long num = Long.parseLong(idPost);
        Key key = KeyFactory.createKey("Post",num);
        Entity post = datastore.get(key);
        //System.out.println(post);
        Entity likeCounter = this.getShardedCounter(post.getKey());
        //System.out.println(likeCounter);
        //System.out.println("------------------------------");
        long NumberOfCpt = (long) likeCounter.getProperty("NumberOfCpt");
        int like=0;
        String str;
        for(int i=0; i<NumberOfCpt; i++){
            str="cpt"+i;
            like += (long) likeCounter.getProperty(str);
        }
        long[] lik = new long[]{like};
        return lik;
    }

    /*
    Obtenir l'entité ShardedLike d'un poste
    */
    private Entity getShardedCounter(Key keyPost){
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query likeCounterQuery = new Query("ShardedLike").setAncestor(keyPost);
        Entity likeCounter = datastore.prepare(likeCounterQuery).asSingleEntity();
        return likeCounter;
    }

    /*
    +1 like on a Post
     */
    @ApiMethod(name = "incrementLike", httpMethod = ApiMethod.HttpMethod.POST)
    public void incrementLike(@Named("idPost") String idPost){
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        long num = Long.parseLong(idPost);
        Key key = KeyFactory.createKey("Post",num);
        Entity likeCounter = getShardedCounter(key);
        long numberCpt = (long) likeCounter.getProperty("NumberOfCpt");
        int rand =new Random().nextInt(toIntExact(numberCpt));
        long cpt = (long) likeCounter.getProperty("cpt"+rand);
        cpt++;
        likeCounter.setProperty("cpt"+rand,cpt);
        datastore.put(likeCounter);
    }


    @ApiMethod(name = "followSomeone", httpMethod = ApiMethod.HttpMethod.POST)
    public Entity followSomeone(@Named("loginFollowed") String loginFollowed, @Named("loginMyAccount") String loginMyAccount) throws NotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Filter filterloginfollowed = new Query.FilterPredicate("login", Query.FilterOperator.EQUAL, loginFollowed);
        Query query = new Query("User").setFilter(filterloginfollowed);

        Filter filterloginmyaccount = new Query.FilterPredicate("login", Query.FilterOperator.EQUAL, loginMyAccount);
        Query query2 = new Query("User").setFilter(filterloginmyaccount);

        Entity userToFollow = datastore.prepare(query).asSingleEntity();
        if (userToFollow == null) {
            throw new NotFoundException("L'utilisateur n'existe pas.");
        }
        Entity userMe = datastore.prepare(query2).asSingleEntity();

        ArrayList<Long> listFollowers = new ArrayList<Long>();
        if (userMe.getProperty("listFollow") != null) {
            listFollowers = (ArrayList<Long>) userMe.getProperty("listFollow");
        }
        Long idToAdd = userToFollow.getKey().getId();
        boolean present = false;
        for (Long id : listFollowers) {
            if (id.equals(idToAdd)) {
                present = true;
            }
        }

        if (!present) {
            listFollowers.add(idToAdd);
            userMe.setProperty("listFollow", listFollowers);
            datastore.put(userMe);
        }
        return userMe;
    }

    @ApiMethod(name = "get10firstUser", httpMethod = ApiMethod.HttpMethod.POST)
    public List<Entity> get10firstUser()
    {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query query = new Query("User").addSort("login");
        List<Entity> list = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(10));
        System.out.println(list);
        return list;
    }

    /*
    TEST SCALING METHOD
     */


    public void testInitiateUser(@Named("nbOfTest") int nbOfTest) throws NotFoundException, EntityNotFoundException {
        this.createUser("user1","user1mdp");
        for(int i = 2;i<nbOfTest+2;i++)
        {
            createUser("user"+i,"user"+i+"mdp");
            followSomeone("user1","user"+i);
        }
        Entity post = newPost("https://statics.lesinrocks.com/content/thumbs/uploads/2019/11/20/1447277/width-1125-height-612-quality-10/babyyoda-width_1014_height_507_x_0_y_3.jpeg","This is a cute yoda ! :)","user1");
    }


    public void testResetLike() throws NotFoundException, EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Filter filterposts = new Query.FilterPredicate("message", Query.FilterOperator.EQUAL, "This is a cute yoda ! :)");
        Query queryposts = new Query("Post").setFilter(filterposts);
        Entity post = datastore.prepare(queryposts).asSingleEntity();
        Entity sh = getShardedCounter(post.getKey());
        long NumberOfCpt = (long) sh.getProperty("NumberOfCpt");
        String str;
        for(int i=0; i<NumberOfCpt; i++){
            str="cpt"+i;
            sh.setProperty(str,0);
        }
        datastore.put(sh);

    }

    public String[] testContention(@Named("nbOfTest") int nbOfTest) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Filter filterposts = new Query.FilterPredicate("message", Query.FilterOperator.EQUAL, "This is a cute yoda ! :)");
        Query queryposts = new Query("Post").setFilter(filterposts);
        Entity post = datastore.prepare(queryposts).asSingleEntity();
        String postid = post.getKey().getId()+"";
        Thread[] th = new Thread[2];
        long startTime = System.currentTimeMillis();
        for(int i = 0;i<th.length;i++)
        {
            final int debut = i*50;
            final int fin = (i+1)*50;
            th[i] = ThreadManager.createThreadForCurrentRequest(new Runnable() {
                public void run() {
                    int cpt=0;
                    for(int j=debut; j<fin;j++)
                    {
                        if (j>0 && j<nbOfTest){
                            String name = "user"+j;
                            try{
                                incrementLike(postid);
                            }
                            catch(Exception e){
                                System.out.println(e);
                            }
                            cpt++;
                        }
                    }
                    System.out.println("thread num "+debut+" a fait : "+cpt);
                }
            });
            th[i].start();
        }
        long endTime = System.currentTimeMillis();
        long[] like = getLikeNumber(postid);
        String[] respond = {"Temps d'exec : " + (endTime - startTime) + " milliseconds","Nombre de like sur le poste : "+like[0]};
        System.out.println(respond[0]);
        return respond;
    }

    // method pour test la vitesse dans l'ajout des postes
    public String[] testNewPost(@Named("url") String url, @Named("newDesc") String newDesc, @Named("author") String pseudo) {
        long startTime = System.currentTimeMillis();
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Filter filterlogin = new Query.FilterPredicate("login", Query.FilterOperator.EQUAL, pseudo);
        Query query = new Query("User").setFilter(filterlogin);
        Entity user = datastore.prepare(query).asSingleEntity();
        Entity poste = new Entity("Post");
        Date date = new Date();
        poste.setProperty("date", date);
        poste.setProperty("idAuteur", user.getKey().getId());
        poste.setProperty("url", url);
        poste.setProperty("message", newDesc);;
        datastore.put(poste);
        newShardedCounter(datastore,poste,28); // permet de créer l'entité pour le like associé au poste
        long endTime = System.currentTimeMillis();
        String[] l =  new String[]{"TIME FOR POST : "+(endTime-startTime)+""};
        return l;
    }


    public String[] testgetTimeline(@Named("login") String login) throws Exception{
        long startTime = System.currentTimeMillis();

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Filter filterlogin = new Query.FilterPredicate("login", Query.FilterOperator.EQUAL, login);
        Query query = new Query("User").setFilter(filterlogin);
        Entity user = datastore.prepare(query).asSingleEntity();
        Long id = user.getKey().getId();

        List<Entity> postList = new ArrayList<Entity>();
        ArrayList<Long> listeF = (ArrayList<Long>) user.getProperty("listFollow");
        for (Long idFollower : listeF){
            Filter filterposts = new Query.FilterPredicate("idAuteur", Query.FilterOperator.EQUAL, idFollower);
            Query queryposts = new Query("Post").setFilter(filterposts);
            List<Entity> posts = datastore.prepare(queryposts).asList(FetchOptions.Builder.withDefaults());
            if(posts != null){
                postList.addAll(posts);
            }
        }
        long endTime = System.currentTimeMillis();
        String[] l =  new String[]{"TIME FOR GETTIMELINE : "+(endTime-startTime)+""};
        return l;
    }

    public void testFollowToutLeMonde() throws Exception{
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        createUser("userQuiFollowToutLeMonde","usermdpp");
        String str;
        for(int i=1; i<200;i++){
            str="user"+i;
            followSomeone(str,"userQuiFollowToutLeMonde");
        }
    }

}
